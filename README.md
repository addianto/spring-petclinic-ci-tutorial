# Spring Petclinic CI Tutorial

[![pipeline status](https://gitlab.com/addianto/spring-petclinic-ci-tutorial/badges/master/pipeline.svg)](https://gitlab.com/addianto/spring-petclinic-ci-tutorial/commits/master)
[![coverage report](https://gitlab.com/addianto/spring-petclinic-ci-tutorial/badges/master/coverage.svg)](https://gitlab.com/addianto/spring-petclinic-ci-tutorial/commits/master)

> A tutorial on CI and TDD, using Spring Petclinic as running example.

TODO

## Authors

- [@addianto](https://gitlab.com/addianto)

## License

Copyright (c) 2018, Daya Adianto

Permission to copy, modify, and share the work in this project is governed under
two licenses: [Apache License 2.0](1) and [Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0)](2).
Unless noted otherwise, Apache License 2.0 applies to the source code (e.g.
Java code, YML configuration files) and CC-BY-SA 4.0 applies to the tutorial
documentation in this project.

[1]: LICENSE
[2]: https://creativecommons.org/licenses/by-sa/4.0/